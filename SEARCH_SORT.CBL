       IDENTIFICATION DIVISION. 
       PROGRAM-ID. SEARCH-SORT.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT 100-FILE-INPUT ASSIGN TO "STUDENT_INPUT.DAT"
              ORGANIZATION IS LINE SEQUENTIAL
              FILE STATUS IS WS-INPUT-FILE-STATUS.
           SELECT 200-OUTPUT-FILE ASSIGN TO "STUDENT_SORTED.DAT"
              ORGANIZATION IS LINE SEQUENTIAL 
              FILE STATUS IS WS-OUTPUT-FILE-STATUS.
           
       DATA DIVISION. 
       FILE SECTION. 
       FD 100-FILE-INPUT
           BLOCK CONTAINS 0 RECORDS.
       01 100-INPUT-RECORD.
          05 100-STU-ID          PIC X(4).
          05 FILLER              PIC X.
          05 100-STU-NAME        PIC X(20).
       FD 200-OUTPUT-FILE
           BLOCK CONTAINS 0 RECORDS.
       01 200-OUTPUT-RECORD      PIC X(30).
       
       WORKING-STORAGE SECTION. 
       01 WS-INPUT-FILE-STATUS   PIC X(2).
          88 FILE-OK                       VALUE "00".
          88 FILE-AT-END                   VALUE "10".
       01 WS-OUTPUT-FILE-STATUS  PIC X(2).
          88 FILE-OK                       VALUE "00".
          88 FILE-AT-END                   VALUE "10".
       01 WS-CALCULATION.
          05 WS-100-INPUT-COUNT  PIC 9(3)  VALUE ZEROS.
          05 WS-SUB              PIC 9(5)  VALUE ZEROS.
          05 WS-ARRAY-MAX        PIC 9(5)  VALUE 7.
       01 WS-ARRAY-STUDENT OCCURS 7
             ASCENDING KEY IS WSA-STU-ID
             INDEXED BY WS-ID-STU.
          05 WSA-STU-ID          PIC X(4).
          05 FILLER              PIC X.
          05 WSA-STU-NAME        PIC X(20).
       PROCEDURE DIVISION .
       0000-MAIN-PROGRAM.
           PERFORM 1000-INTIAL THRU 1000-EXIT
           PERFORM 2000-PROCESS THRU 2000-EXIT
           PERFORM 3000-END THRU 3000-EXIT
           GOBACK.

       1000-INTIAL.
           OPEN INPUT 100-FILE-INPUT
           OPEN OUTPUT 200-OUTPUT-FILE
           PERFORM 8000-READ THRU 8000-EXIT 
           PERFORM 4000-LOAD-STUDENT THRU 4000-EXIT 
           .
       1000-EXIT.
           EXIT.

       2000-PROCESS.
           PERFORM 2100-PRINT-ARRAY-STUDENT THRU 2100-EXIT 
           SET WS-ID-STU TO 1
           SEARCH WS-ARRAY-STUDENT 
           AT END
              DISPLAY "NOT FOUND!!!"
           WHEN WSA-STU-ID(WS-ID-STU) = "9056"
                DISPLAY "FOUNDED "
                        WS-ARRAY-STUDENT(WS-ID-STU)
                        " AT "
                        WS-ID-STU
           END-SEARCH

           SET WS-ID-STU TO 1
           SEARCH WS-ARRAY-STUDENT 
           AT END
              DISPLAY "NOT FOUND!!!"
           WHEN WSA-STU-NAME(WS-ID-STU) = "James Smith"
                DISPLAY "FOUNDED "
                        WS-ARRAY-STUDENT(WS-ID-STU)
                        " AT "
                        WS-ID-STU
           END-SEARCH

           SORT WS-ARRAY-STUDENT ASCENDING WSA-STU-ID 
           DISPLAY "SORT BY ID"
           PERFORM 2100-PRINT-ARRAY-STUDENT THRU 2100-EXIT 

           SET WS-ID-STU TO 1
           SEARCH ALL WS-ARRAY-STUDENT 
           AT END
              DISPLAY "NOT FOUND!!!"
           WHEN WSA-STU-ID(WS-ID-STU) = "9056"
                DISPLAY "FOUNDED "
                        WS-ARRAY-STUDENT(WS-ID-STU)
                        " AT "
                        WS-ID-STU
           END-SEARCH

           PERFORM VARYING WS-SUB FROM 1 BY 1
              UNTIL WS-SUB > WS-ARRAY-MAX
                   MOVE WS-ARRAY-STUDENT(WS-SUB) TO 200-OUTPUT-RECORD 
                   PERFORM 7000-WRITE THRU 7000-EXIT 
           END-PERFORM
           .
           
       2000-EXIT.
           EXIT.

       2100-PRINT-ARRAY-STUDENT.
           PERFORM VARYING WS-SUB FROM 1 BY 1
              UNTIL WS-SUB > WS-ARRAY-MAX
                   DISPLAY WS-ARRAY-STUDENT(WS-SUB)
           END-PERFORM
           .
       2100-EXIT.
           EXIT.

       3000-END.
           DISPLAY "READ: " WS-100-INPUT-COUNT " RECORDS"
           CLOSE 100-FILE-INPUT
           .
       3000-EXIT.
           EXIT.

       4000-LOAD-STUDENT.
           PERFORM VARYING WS-SUB FROM 1 BY 1
              UNTIL FILE-AT-END OF WS-INPUT-FILE-STATUS
              OR WS-SUB > WS-ARRAY-MAX
                   MOVE 100-INPUT-RECORD TO WS-ARRAY-STUDENT(WS-SUB)
                   PERFORM 8000-READ THRU 8000-EXIT 
           END-PERFORM
           .
       4000-EXIT.
           EXIT.

       7000-WRITE.
           WRITE 200-OUTPUT-RECORD 
           IF FILE-OK OF WS-OUTPUT-FILE-STATUS 
              CONTINUE
           ELSE
              DISPLAY "***** SEARCH-SORT ANEND *****"
                 UPON CONSOLE 
              DISPLAY "* PARA 7000-WRITE FAIL *"
                 UPON CONSOLE 
              DISPLAY "* FILE STATUS: " WS-OUTPUT-FILE-STATUS " *"
                 UPON CONSOLE 
              DISPLAY "***** SEARCH-SORT ANEND *****"
                 UPON CONSOLE 
              STOP RUN 
           END-IF 
           . 
       7000-EXIT.
           EXIT.

       8000-READ.
           READ 100-FILE-INPUT
           IF FILE-OK OF WS-INPUT-FILE-STATUS 
              ADD 1 TO WS-100-INPUT-COUNT 
           ELSE
              IF FILE-AT-END OF WS-INPUT-FILE-STATUS 
                 CONTINUE
              ELSE
                 DISPLAY "***** SEARCH-SORT ANEND *****"
                    UPON CONSOLE 
                 DISPLAY "* PARA 8000-READ FAIL *"
                    UPON CONSOLE 
                 DISPLAY "* FILE STATUS: " WS-INPUT-FILE-STATUS " *"
                    UPON CONSOLE 
                 DISPLAY "***** SEARCH-SORT ANEND *****"
                    UPON CONSOLE 
                 STOP RUN 
              END-IF 
           END-IF 
           .
       8000-EXIT.
           EXIT.